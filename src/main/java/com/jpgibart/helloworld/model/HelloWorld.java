package com.jpgibart.helloworld.model;

public class HelloWorld {
	private String value = "Hello World!";
	@Override
	public String toString() {
		return this.value;
	}
}
