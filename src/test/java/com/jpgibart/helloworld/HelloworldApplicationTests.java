package com.jpgibart.helloworld;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.jpgibart.helloworld.model.HelloWorld;
import com.jpgibart.helloworld.service.BusinessService;

@SpringBootTest
class HelloworldApplicationTests {

	@Autowired
	private BusinessService bs;
	
	@Test
	void contextLoads() {
	}
	
	public void testGetHelloWorld() {
		String expected = "Hello World!";
		String result = bs.getHelloWorld().toString();
		assertEquals(expected, result);
		
	}

}
